package lab9;

import lab9.providers.ProvidesColor;
import lab9.providers.ProvidesGuess;
import lab9.providers.ReceivesHistory;
import sedgewick.StdDraw;

public class VisualizeGame implements ReceivesHistory {

	public GameProperties newGame;
	public ProvidesGuess codeBreaker;
	public ProvidesGuess codeMaker;
	public ProvidesColor colors; 
	private History history; 
	
	public VisualizeGame(GameProperties newGame, ProvidesGuess codeBreaker, ProvidesGuess codeMaker, ProvidesColor colors) {
		this.newGame = newGame;
		this.codeBreaker = codeBreaker;
		this.codeMaker = codeMaker; 
		this.colors = colors; 
		this.history = null; 
	}

	public void drawBasicGameBoard () { 

		StdDraw.setPenColor(StdDraw.CYAN);
		
		this.newGame.getMaxNumGuesses();
		this.newGame.getNumHoles(); 
	
		double intervalY = 1.0/(double)(this.newGame.getMaxNumGuesses()+1);
		double intervalX = 1.0/(double)(this.newGame.getNumHoles()+1);

		for (double i = intervalX; i < 1.0; i += intervalX){
			for (double j = intervalY; j < 1.0; j += intervalY){
			StdDraw.filledCircle(i - 0.02, j - 0.02, 0.02);
			}
		}
				
	}
	
	public void drawFirstGuess () { 
				
		double intervalX = 1.0/(double)(this.newGame.getNumHoles()+1);
		
		double position = 0; 
		
		int [] guess = this.codeBreaker.getGuess().guessArray; 
						
		for (int i = 0; i < this.newGame.getNumHoles(); i++) {
			
			StdDraw.setPenColor(this.colors.getColorForPeg(guess[i]));
			
			StdDraw.filledCircle(position + intervalX, 0.5, 0.02);
			
			position += intervalX;
			
		}
			
	}
	
	public void drawOfficialGame () {
		
		// if history = null fill it with first guess
				
		double intervalX = 1.0/(double)(this.newGame.getNumHoles()+1);
		double intervalY = 1.0/(double)(this.newGame.getMaxNumGuesses());
		double positionX = 0; 
		double positionY = 0.95; 
		
		int [] guess = this.codeBreaker.getGuess().guessArray;
		
		for (int i = 0; i < this.newGame.getMaxNumGuesses(); i++) {
			for (int j = 0; j < this.newGame.getNumHoles(); j++) {
				StdDraw.setPenColor(this.colors.getColorForPeg(guess[j]));
				StdDraw.filledCircle(positionX + intervalX, positionY, 0.02);
				positionX += intervalX;
			}
			guess = this.codeBreaker.getGuess().guessArray;
			positionY -= intervalY;
			positionX = 0; 
		}
		
	}
	
	@Override
	public void sendHistory(History history) {

		this.history = history; 
		
		//When calling this just use history
		
		//System.out.println("Received a new history: " + history.toString());
		
		double intervalX = 0.5/(double)(this.newGame.getNumHoles()+1);
		double intervalY = 1.0/(double)(this.newGame.getMaxNumGuesses());
		double positionX = 0.05; 
		double positionY = 1.0; 
		
		Guess currentGuess = history.getHistoryAt(history.size() - 1);
				
		for (int j = 0; j < this.newGame.getNumHoles(); j++) {
			StdDraw.setPenColor(this.colors.getColorForPeg(currentGuess.guessArray[j]));
			StdDraw.filledCircle(positionX, positionY - intervalY*history.size(), 0.02);
			positionX += intervalX;
		}
		
	}
	
}
